import java.util.Scanner;

public class Vector {

    public static void main(String[] args){
        int n;
    System.out.println("Introduce the number of elements n =");

        //Citim numarul de elemente
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        //Declaram un vector "v" de "n" elemente.
        int[] v = new int[n];

        // Citim cele n elemente ale vectorului
        for (int i = 0; i < n; i++) {
            System.out.print("V[" + i + "]=");
            v[i] = sc.nextInt();
        }
        // Afisam elementele vectorului
        for (int elemente : v) {
            System.out.println(elemente);
        }
    }
}

